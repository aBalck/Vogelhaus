#!/bin/bash

VENV=.birdhouse_venv
if [ -e "$VENV" ]; then
	echo "$VENV exists."

	echo "activate $VENV"
	source "$VENV/bin/activate"
else 
	echo "$VENV does not exist -> it will be created"
	python -m venv --system-site-packages $VENV
	
	echo "activate $VENV"
	source "$VENV/bin/activate"

	echo "install gloabel dependencies"
	sudo apt install -y python3-libcamera python3-kms++
	sudo apt install -y python3-prctl libatlas-base-dev ffmpeg python3-pip
	sudo apt install -y python3-pyqt5 python3-opengl 

	echo "install all dependencies in $VENV"
	pip3 install --upgrade Pillow
	pip3 install guizero
	pip3 install numpy
	pip3 install picamera2[gui]
fi

echo "perpare QTGL"
chmod 0700 /run/user/1000

echo "start Vogelhaus-Ui"
cd src
python3 my_gui.py

