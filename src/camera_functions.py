from picamera import PiCamera
camera=PiCamera()
from time import sleep
from datetime import datetime
import threading

import my_gui

global preview_thread
global interval_thread
global stop_interval

def preview_on():
    camera.start_preview(fullscreen=False, window =(800, 50, 800, 280))
    
def preview_off():
    camera.stop_preview()
    
def timed_preview(time):
    text = str(time) +" seconds preview"
    print(text)
    camera.start_preview(fullscreen=False, window =(800, 50, 800, 280))
    #camera.start_preview()
    global preview_thread
    preview_thread = threading.Thread(target=stop_preview, args=(time,), daemon= True)
    preview_thread.start()
    
def short_preview():
    timed_preview(5)

def long_preview():
    timed_preview(15)
    
def stop_preview(time):
    print("stop_preview in " +str(time))
    sleep(time)
    camera.stop_preview()

def auto_naming():
    path = "../aufzeichnungen/Vogelhaus_"
    date_now = datetime.now().strftime("%d-%m-%Y_%H-%M-%S")
    return (path + date_now, date_now)

def start_photo_interval(interval):
    global interval_thread
    global stop_interval
    stop_interval = False
    interval_thread = threading.Thread(target=perform_photo_interval, args=(interval,), daemon= True)
    interval_thread.start()

def stop_photo_interval():
    global stop_interval
    stop_interval = True
    
def perform_photo_interval(interval):
    global stop_interval
    while not stop_interval:
        capture_image_no_preview()
        sleep(int(interval) -1)

def capture_image_no_preview():
    global capture_number
    print("image captured")
    name, date = auto_naming()
    camera.annotate_text = (date)
    camera.capture(name +'.png')
    return name +'.png'

def capture_image():
    global capture_number
    print("image captured")
    camera.start_preview()
    # Camera warm-up time
    sleep(1)
    name, date = auto_naming()
    camera.annotate_text = (date)
    camera.capture(name +'.png')
    camera.stop_preview()
    return name +'.png'
    
    
def video_capture(length):
    print("video wird " +str(length) +" Sekunden aufgenommen")
    length = int(length)
    camera.resolution = (800, 480)
    camera.start_preview(fullscreen=False, window =(800, 50, 800, 280))
    name, date = auto_naming()
    camera.annotate_text = (date)
    camera.start_recording(name +'.Video.h264')
    camera.wait_recording(length)
    camera.stop_recording()
    camera.stop_preview()

