from guizero import App,PushButton, Text, Box, Picture, Slider, CheckBox
from guizero import info
from subprocess import check_call

import camera2_functions as cameraFunc

global preview_on
global viewer
global video_length
global photo_interval_length
global photo_interval_on

    
def shutdown():
    #check_call(['sudo', 'poweroff'])
    close_down = app.yesno("SHUTDOWN", "Are you sure?")
    if close_down == True:
        app.warn("Ready", "Shutting down")
        check_call(['sudo', 'poweroff'])
    else:
        app.info("Shutdown", "Shutdown aborted")
    
def press_preview_on():
    global preview_on
    if preview_on:
        preview_on = False
        print("Durchgehende Vorschau deaktiviert")
        cameraFunc.preview_off()
        button_preview_on.bg="green"
        button_preview_on.text = "Dauerhafte Vorschau \n anschalten"
        short_preview.show()
        long_preview.show()
        ### pop up are you sure
        #always = app.yesno("Check", "Are you sure? Other features cannot be used")
        #thread = threading.Thread(target=cameraFunc.always_on, args=(), daemon=True)
        
    else:
        preview_on = True
        print("Durchgehende Vorschau aktiviert")
        cameraFunc.preview_on()
        button_preview_on.bg="red"
        button_preview_on.text = "Dauerhafte Vorschau \n ausschalten"
        short_preview.hide()
        long_preview.hide()
        
def take_and_show_pic():
    global preview_on
    if preview_on:
        press_preview_on()
    
    img_path = cameraFunc.capture_image()
    viewer.image = img_path
    viewer.show()
    picture_text.show()
    
def change_video_slider(new_value):
    global video_length
    video_length = new_value
    video.update_command(cameraFunc.video_capture, args=[video_length])
    
def change_interval_slider(new_value):
    global photo_interval_length
    photo_interval_length = new_value
    
def switch_interval_photo():
    global photo_interval_length
    global photo_interval_on
    photo_interval_on = not photo_interval_on
    
    if(photo_interval_on):
        interval_photo.bg = "red"
        interval_photo.text= "Foto-Intervall \n deaktivieren"
        if interval_in_min.value == 1:
            print("Intervall-Fotos alle " +str(photo_interval_length)+ " Minuten aktiviert")
            interval_in_seconds = int(photo_interval_length) * 60
            cameraFunc.start_photo_interval(interval_in_seconds)
        else:
            print("Intervall-Fotos alle " +str(photo_interval_length)+ " Sekunden aktiviert")
            cameraFunc.start_photo_interval(photo_interval_length)
            
    else:
        print("Intervall-Fotos deaktiviert")
        cameraFunc.stop_photo_interval()
        interval_photo.bg = "green"
        interval_photo.text= "Foto-Intervall \n aktivieren"
        
if __name__ == '__main__':
    photo_interval_on = False
    photo_interval_length = 5
    video_length = 5
    preview_on = False
    
    app = App(title = 'Vogelhaus', width = "850", height = "1070")
    app.bg = "white"
    app.text_color = "black"
    #app.tk.attributes("-fullscreen", True)
    welcome_box = Box(app)
    bird_pic= Picture(welcome_box, "undraw_happy_music_g6wc.png", width=224, height=186, align= "left")
    discription_text = Text(app, "Vogelhaussteuerung", font = "Posterama", size= 22)
    
    controls_preview = Box(app, align="top")
    preview_box_discription_text = Text(controls_preview, "Guck dir an was gerade im Vogelhaus passiert")
    short_preview = PushButton(controls_preview, text="Kurze Vorschau", width = "fill", height = "fill", align = "right", command=cameraFunc.short_preview) 
    long_preview = PushButton(controls_preview, text="Lange Vorschau", width = "fill", height = "fill", align = "right", command=cameraFunc.long_preview) 
    button_preview_on = PushButton(controls_preview, text="Dauerhafte Vorschau \n anschalten", width = "fill", height = "fill", align = "right", command=press_preview_on)
    button_preview_on.bg="green"
    
    controls_save = Box(app, align="top")
    discription_spacing = Text(controls_save, " ")
    discription_text = Text(controls_save, "Speichere Videos oder Bilder")
    
    photo_box = Box(controls_save, border =1, align= "left", height = 100, width = 400)
    single_image = PushButton(photo_box, text="Einzelnes Foto \n aufnehmen", height = "fill", align = "right", command=take_and_show_pic)
    interval_photo_slider_box = Box(photo_box , align = "left")
    interval_photo_slider_text = Text(interval_photo_slider_box, "Intervall", size= 10)
    interval_in_min = CheckBox(interval_photo_slider_box, text="in Minuten")
    interval_photo_slider = Slider(interval_photo_slider_box, start = 5, end= 120, command= change_interval_slider)
    interval_photo = PushButton( photo_box, text="Intervall-Fotos \n aktivieren", height = "fill", align = "left", command=switch_interval_photo)
    interval_photo.bg = "green"
    
    
    
    video_box = Box(controls_save, border =1, align= "left" , height = 100 , width = 280)
    video = PushButton(video_box, text="Video \n aufnehmen", height = "fill", align = "right", command=cameraFunc.video_capture, args=[video_length])
    video_slider_box = Box(video_box)
    video_slider_text = Text(video_slider_box, "Video-Dauer in Sekunden", size= 10)
    video_slider = Slider(video_slider_box, start = 5, end= 120, command= change_video_slider)
    
    
    picture_box = Box(app)
    picture_spacing = Text(picture_box, text= " ")
    picture_text = Text(picture_box, text= "letztes gespeichrtes Bild")
    picture_text.hide()
    viewer = Picture(picture_box, width=800, height=480)
    
    shutdown = PushButton(app, text="Shutdown pi", align = "bottom", command=shutdown)
    shutdown.bg ="red"

    
    app.display()
