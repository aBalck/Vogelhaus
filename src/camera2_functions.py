from time import sleep
from datetime import datetime
import threading
import os
from picamera2 import Picamera2, Preview
from picamera2.encoders import H264Encoder
import my_gui

picam2 = Picamera2()
preview_config = picam2.create_preview_configuration()
capture_config = picam2.create_still_configuration()
video_config = picam2.create_video_configuration()

picam2.configure(preview_config)

global preview_thread
global interval_thread
global stop_interval

def preview_on():
    preview_off()
    try:
        picam2.configure(preview_config)
    except:
        print("already in preview_config")
    picam2.start(show_preview=True)
    

def preview_off():
    try:
        picam2.stop_preview()
        picam2.stop()
    except:
        print("all previews stopped")
    
    
def timed_preview(time):
    text = str(time) +" seconds preview"
    print(text)
    preview_on()
    global preview_thread
    preview_thread = threading.Thread(target=stop_preview, args=(time,), daemon= True)
    preview_thread.start()
    
def short_preview():
    timed_preview(5)

def long_preview():
    timed_preview(15)
    
def stop_preview(time):
    print("stop_preview in " +str(time))
    sleep(time)
    preview_off()

def auto_naming():
    path = "Vogelhaus_"
    date_now = datetime.now().strftime("%d-%m-%Y_%H-%M-%S")
    return (path + date_now, date_now)

def start_photo_interval(interval):
    global interval_thread
    global stop_interval
    stop_interval = False
    interval_thread = threading.Thread(target=perform_photo_interval, args=(interval,), daemon= True)
    interval_thread.start()

def stop_photo_interval():
    global stop_interval
    stop_interval = True
    
def perform_photo_interval(interval):
    global stop_interval
    while not stop_interval:
        capture_image()
        sleep(int(interval) -1)

def capture_image():
    preview_off()
    print("image captured")
    name, date = auto_naming()
    filename = name + ".png"
    try:
        picam2.configure(preview_config)
    except:
        print("already in preview_config")
    picam2.start(show_preview=True)
    sleep(1)
    picam2.switch_mode_and_capture_file(capture_config, filename)
    preview_off()
    print("saving " + filename)
    pathToImg = move_file_to_recordings(filename)
    return pathToImg
    
    
def video_capture(length):
    preview_off()
    print("video wird " +str(length) +" Sekunden aufgenommen")
    length = int(length)
    name, date = auto_naming()
    filename = name + ".h264"
    try:
        picam2.configure(video_config)
    except:
        print("already in video_config")
    encoder = H264Encoder(10000000)
    picam2.start_encoder(encoder, filename)
    picam2.start(show_preview=True)
    sleep(length)
    print("saving " + filename)
    picam2.stop_recording()
    preview_off()
    move_file_to_recordings(filename)
    
def move_file_to_recordings(filename):
    newPath = "../recordings/" + filename
    os.replace(filename, newPath)
    return newPath

short_preview()
