--- English version below---
# Smartes Vogelhaus
In diesem Projekt wurde ein Vogelhaus entwickelt, das mithilfe einer integrierten Kamera die Vögel beim Nisten beobachten kann.  

## Software
Dieses Repo enthält eine Software, durch die das Vogelhaus per GUI gesteuert werden kann.
Die GUI ist auf Deutsch, sollte sich aber mit einer Übersätzungstool leicht anpassen lassen.

![gui](./readme-images/birdhouse-gui.png)

Die Steuerung ermöglicht es, eine Live-Vorschau zusehen.
Außerdem können sowohl Fotos als auch Videos aufgenommen werden.
Des Weiteren ist es möglich, ein Intervall einzustellen, in dem automatische Bilder erstellt werden.
Bild und Videos werden mit dem aktuellen Datum und Uhrzeit beschriftet und im Ordner „Aufnahmen“ gespeichert.

Zum Ausführen des Programms wird python3 benötigt.
Alle benötigten Bibliotheken werden in einer viruellen Umgebung automatisch bei dem Starten des Programms installiert.
Zum Starten des Programm muss folgender Befehl im Repository ausgeführt werden.
```shell
./run.sh
```

Aktuell ist das Programm in zwei Versionen verfügbar. Es kann entweder die Bibliothek picamera oder picamera2 genutzt werden.
Um picamera zu nutzen, muss entweder das Programm leicht angepasst werden, oder auf den Commit 7d40377c680f3a438b0575e10c84311ac81ea99b zurückgespurngen werden.

## Hardware
### Benötigte Dinge
- Raspberry Pi
- Raspberry Pi Nachtsicht-Kamera
- Garten-Stromkabel
- Netzteil Raspberry Pi
- Holz

### Aufbau
Der folgende Abschnitt soll nur zu Orientierung dienen und ist keine exakte Aufbauanleitung.  
Das Haus hat eine doppelte Decke, in dem Zwischenraum befindet sich die Elektronik.  
Für die Kamera werden Löcher in die Zwischendecke gebohrt, damit sie nach unten filmen kann. 

![birdhouse-dimensions](./readme-images/birdhouse-concept-drawing.png)
![photo1](./readme-images/birdhous-top.jpg)
![photo2](./readme-images/birdhouse-front-and-side.jpg)


## Montage
Bevor das Vogelhaus aufgehängt werden kann, sollte der Raspberry Pi eingerichtet und dieses Projekt zum Laufen gebracht werden.
Es empfiehlt sich, den Raspberry Pi mit einem WLAN zu verbinden und beispielsweise per NVC Viewer auf ihn zuzugreifen.
Dies ermöglicht auch nach der Montage einen Zugriff und Steuerung.  
Als Standort für das Vogelhaus sollte ein Ort gewählt werden, der durch ein Garten-Stromkabel mit Strom versorgt werden kann und bei dem es das WLAN noch stark genug ist.  

<br/>
<br/>
--- English version ---

# Smart bird house
In this project a bird house was developed, which can observe the birds nesting with the help of an integrated camera.  

## Software
This repo contains software to control the birdhouse via GUI.
The GUI is in German, but should be easy to customize with an overetch tool.

The control allows you to see a live preview.
In addition, both photos and videos can be recorded.
Furthermore, it is possible to set an interval in which images are automatically created.
Pictures and videos are labeled with the current date and time and saved in the "Aufnahmen" (recordings) folder.

To run the program python3 is needed.
All required libraries are automatically installed in a virtual environment when the program is started.
To start the program, the following command must be executed in the repository.
```shell
./run.sh
```

The program is currently available in two versions. Either the picamera or picamera2 library can be used.
To use picamera, either the program must be slightly adapted or the commit 7d40377c680f3a438b0575e10c84311ac81ea99b must be reverted to.

## Hardware
### Things needed
- Raspberry Pi
- Raspberry Pi night vision camera
- Garden power cable
- Power supply Raspberry Pi
- Wood

### Assembly
The following section is for orientation only and is not an exact assembly instruction.  
The house has a double ceiling, and the electronics are located in the space between.  
Holes are drilled in the false ceiling for the camera so that it can film downwards. 


## Setup
Before the birdhouse can be hung, the Raspberry Pi should be set up and this project should be made to work.
It is recommended to connect the Raspberry Pi to a WLAN and access it via NVC Viewer for example.
This allows access and control even after installation.  
As a location for the birdhouse, a place should be chosen that can be supplied with power by a garden power cable and where the WLAN is still strong enough.
